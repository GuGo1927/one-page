# VueJS Docker Container

## Build Setup

``` bash
# setup container
docker-compose up --build

# Run container
docker-compose up
```