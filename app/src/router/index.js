import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/containers/index'
import ContainerLogin from '@/containers/login'
import ContainerGlobalMarket from '@/containers/global.market'
import ContainerRegister from '@/containers/register'
import ForgotPassword from '@/containers/forgot.password'
import ContainerConfirm from '@/containers/confirm'
import NotFound from '@/containers/NotFound'

import AccountAPi from '@/containers/account/api';
import AccountAuthorizationHistory from '@/containers/account/authorization.history';
import AccountFinance from '@/containers/account/finance';
import AccountGeneralSettings from '@/containers/account/general.settings';
import AccountNotification from '@/containers/account/notification';
import AccountReferralProgramm from '@/containers/account/referral.programm';
import AccountSecurity from '@/containers/account/security';
import AccountUserSettings from '@/containers/account/user.settings';
import AccountVerification from '@/containers/account/varification';

import { store } from '@/stores/store';

import * as UserServiceAction from '@/stores/modules/user/types';

Vue.use (Router);

let router = new Router ({
	                           routes: [
		                           {
			                           path: '/',
			                           name: 'home',
			                           component: Home
		                           },
		                           {
			                           path: '/sign-in',
			                           name: 'sign-in',
			                           component: ContainerLogin,
			                           meta: { closeForAuth: true }
		                           },
		                           {
			                           path: '/forgot.password',
			                           name: 'forgot',
			                           component: ForgotPassword,
			                           meta: { closeForAuth: true }
		                           },
		                           {
			                           path: '/new.password/:hash',
			                           alias: '/new.password/secret/:hash',
			                           name: 'new-password',
			                           component: ForgotPassword,
			                           props: true,
			                           meta: { closeForAuth: true }
		                           },{
			                           path: '/new.password/:time/:hash',
			                           alias: '/new.password/secret/:time/:hash',
			                           name: 'new-password',
			                           component: ForgotPassword,
			                           props: true,
			                           meta: { closeForAuth: true }
		                           },
		                           {
			                           path: '/markets',
			                           name: 'markets',
			                           component: ContainerGlobalMarket
		                           },
		                           {
			                           path: '/account',
			                           alias: '/account/user.settings',
			                           component: AccountUserSettings,
			                           name: 'account_user_settings',
			                           meta: { requiresAuth: true },
		                           },
		                           {
			                           path: '/account/verification',
			                           name: 'account_verification',
			                           component: AccountVerification,
			                           meta: { requiresAuth: true },
		                           },
		                           {
			                           path: '/account/security',
			                           name: 'account_secutiry',
			                           component: AccountSecurity,
			                           meta: { requiresAuth: true },
		                           },
		                           {
			                           path: '/account/finance',
			                           name: 'account_finance',
			                           component: AccountFinance,
			                           meta: { requiresAuth: true },
		                           },
		                           {
			                           path: '/account/referral.programm',
			                           name: 'account_referral_programm',
			                           component: AccountReferralProgramm,
			                           meta: { requiresAuth: true },
		                           },
		                           {
			                           path: '/account/api',
			                           name: 'account_api',
			                           component: AccountAPi,
			                           meta: { requiresAuth: true },
		                           },
		                           {
			                           path: '/account/authorization.history',
			                           name: 'account_authorization_history',
			                           component: AccountAuthorizationHistory,
			                           meta: { requiresAuth: true },
		                           },
		                           {
			                           path: '/account/general.settings',
			                           name: 'account_general_settings',
			                           component: AccountGeneralSettings,
			                           meta: { requiresAuth: true },
		                           },
		                           {
			                           path: '/account/notification',
			                           name: 'account_notification',
			                           component: AccountNotification,
			                           meta: { requiresAuth: true },
		                           },
		                           {
			                           path: '/markets/:coin',
			                           name: 'markets/coin',
			                           component: ContainerGlobalMarket,
			                           props: true
		                           },
		                           {
			                           path: '/logout',
			                           name: 'logout',
			                           redirect: to => {

				                           Vue.cookie.delete ('token', { domain: document.domain });
				                           store.dispatch('user/A_LOGOUT');

				                           return { path: '/sign-in' };
			                           }
		                           },
		                           {
			                           path: '/registration-confirm/:hash',
			                           name: 'registration-confirm',
			                           component: ContainerConfirm,
			                           props: true
		                           },
		                           {
			                           path: '/sign-up',
			                           name: 'sign-up',
			                           component: ContainerRegister,
			                           meta: { closeForAuth: true }
		                           },
		                           {
			                           path: '*',
			                           component: NotFound
		                           }
	                           ]
                           });

router.beforeEach((to, from, next) => {

	const token = Vue.cookie.get ('token', { domain: document.domain });

	var ValidateRouter = () => {

		if (to.matched.some(record => record.meta.requiresAuth)) {

			if (store.state.user.isAuthenticated == false) {
				next({
					     path: '/sign-in',
					     query: { redirect: to.fullPath }
				     })
			} else {
				next()
			}
		}  else if (to.matched.some(record => record.meta.closeForAuth)) {

			if (store.state.user.isAuthenticated != false  && store.state.user.isAuthenticated != undefined) {
				next({
					     path: '/'
				     })
			} else {
				next()
			}
		} else {
			next();
		}

		store.commit('Complete');
	};

	if(token != undefined && token != "" && store.state.user.isValidateToken == false)
	{
		Vue.token.check(token).then((result) => {

			console.log("Is Validate Token");
			console.log(result);

			if(result.isValid == true)
			{
				store.dispatch('user/'+UserServiceAction.LOGIN_FROM_COOKIE, result.response);
			}

			if(result.isExpired == true)
			{
				Vue.cookie.delete ('token', { domain: document.domain });
				store.dispatch('user/'+UserServiceAction.A_LOGOUT);
			}

			ValidateRouter();
		});
	} else {
		ValidateRouter();
	}
});

export default router;