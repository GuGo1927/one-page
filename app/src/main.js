// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

import VueCookie from 'vue-cookie';
Vue.use(VueCookie);

import router from './router'
import vuexI18n from 'vuex-i18n';

require('./libs/setup.plugins');

Vue.config.productionTip = false;

import { store } from './stores/store';

import LangEn from './translations/en';
import LangRu from './translations/ru';

Vue.use(vuexI18n.plugin, store);

Vue.i18n.add('en', LangEn);
Vue.i18n.add('ru', LangRu);

Vue.i18n.set('en');

new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  store
})
