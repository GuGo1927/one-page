import * as UserServiceActionTypes from '@/stores/modules/user/types';
import UserServiceAction from '@/stores/modules/user/actions';
import Promise from 'promise';

const TokenValidatorPlugin = {
	
	install(Vue, options){

		Vue.token = Vue.prototype.$token = {
			
			get(){
				
				if(!Vue.hasOwnProperty('cookie')){
					return;
				}
				
				const cookieName  = options.cookie || 'token';
				return Vue.cookie.get(cookieName);
			},
			
			check(token){

				console.log("CHECK");
				return new Promise (( resolve, reject ) => {

					const serviceUrl  = options.service_url || 'http://localhost';

					let validate_result = { isValid: false, isExpired: false, response: {} };

					if(token != undefined && token != null && token != "")
					{
						console.log("CHECK 4");

						Vue.Api.Post(serviceUrl+"/validate", null, { headers: { 'Authorization' : 'Bearer '+token } }).then((result) => {

							if(result.status == 401)
							{
								validate_result.isExpired = true;
								//Vue.store.dispatch('user/'+UserServiceActionTypes.A_LOGOUT);
							} else {
								validate_result.isValid = true;
								validate_result.response = { token: token, user: result.data };
							}

							console.log(result);

							resolve(validate_result);

						}, (error) => {

							console.log("/validate error");
							validate_result.isExpired = true;

							resolve(validate_result);
						});
					} else {
						console.log("CHECK 3 EXIT");
						resolve(validate_result);
					}

				});
			}
		};
	}
};

export default TokenValidatorPlugin;