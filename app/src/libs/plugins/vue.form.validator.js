import Validator from 'validate.js'

const VueFormValdator = {

    install(Vue, options){

        Vue.prototype.$Validator = function(values, constraints, options)
        {
            return Validator.async(values, constraints, options);
        }

    }

}

export default VueFormValdator;
