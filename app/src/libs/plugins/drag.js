import Promise from 'promise';

const DragManager = {
	
	install(Vue, options){
		
		console.log("INSTALL DragManager");
		
		Vue.prototype.$DragManager = {
			dragged : null,
		};
		
		/*document.addEventListener("drag", function( event ) {
		
		}, false);*/
		
		document.addEventListener("dragstart", function( event ) {
			
			console.log("dragstart");
			
			Vue.prototype.$DragManager.dragged = event.target;
			event.target.style.opacity = .75;
			
		}, false);
		
		document.addEventListener("dragend", function( event ) {
			// reset the transparency
			event.target.style.opacity = "";
			Vue.prototype.$DragManager.dragged = null;
			
		}, false);
		
		/* events fired on the drop targets */
		document.addEventListener("dragover", function( event ) {
			event.preventDefault();
		}, false);
		
		document.addEventListener("dragenter", function( event ) {
			
			if ( event.target.className.indexOf("dropzone") > -1 ) {
				event.target.__vue__.OnEnter();
				
				console.log(event);
			}
			
			//console.log("dragenter");
			
		}, false);
		
		document.addEventListener("dragleave", function( event ) {
			// reset background of potential drop target when the draggable element leaves it
			
			
			if ( event.target.className.indexOf("dropzone") > -1 ) {
				console.log(event);
				event.target.__vue__.OnLeave();
			}
			
			//console.log("dragleave");
			
		}, false);
		
		document.addEventListener("drop", function( event ) {
			// prevent default action (open as link for some elements)
			event.preventDefault();
			// move dragged elem to the selected drop target
			if ( event.target.className == "dropzone" ) {
				event.target.style.background = "";
				dragged.parentNode.removeChild( dragged );
				event.target.appendChild( dragged );
			}
			
			//console.log("drop");
			
		}, false);
	}
	
}

export default DragManager;
