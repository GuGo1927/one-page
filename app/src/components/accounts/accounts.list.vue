<template>

    <div class="table-backgound has-text-left">
        <div class="columns">
            <div class="column block-small-title flex-centered-left">
                {{ 'Overview' | translate }}
            </div>

            <div class="column is-four-fifths flex-centered-right small-header-text">
                <div>
                    <label class="checkbox">
                        <input type="checkbox" @click="hideZero">
                        {{ 'Hide zero balance' | translate }}
                    </label>
                </div>
                <div class="field has-addons">
                    <div class="control">
                        <input class="input invisible-input" v-model="filterByCoin" type="text" :placeholder="$t('Write for search')">
                    </div>
                    <div class="control search">
                        &nbsp;
                    </div>
                    <!--div class="control">
                        <a @click="filterByCoin=''" class="button">
                            Clear
                        </a>
                    </div-->
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column has-text-center block-header-line">
                <img src="../../assets/block-header-line.png">
            </div>
        </div>
        <div class="columns">
            <div class="column">

                <table class="table is-hoverable is-fullwidth">
                    <thead v-if="filterByCoin.length == 0">
                    <tr>
                        <th><a :class="{'selected-table-title': sortBy == 'title'}" @click.prevent="sort('title', $event)" href="#" v-html="$t('Coin')"></a>&nbsp;<span class="icon"><i :class="sortArrow('title')"></i></span></th>
                        <th><a :class="{'selected-table-title': sortBy == 'active_balance'}" @click.prevent="sort('active_balance', $event)" href="#" v-html="$t('Available amount')"></a>&nbsp;<span class="icon"><i :class="sortArrow('active_balance')"></i></span></th>
                        <th><a :class="{'selected-table-title': sortBy == 'frozen_balance'}" @click.prevent="sort('frozen_balance', $event)" href="#" v-html="$t('Frozen amount')"></a>&nbsp;<span class="icon"><i :class="sortArrow('frozen_balance')"></i></span></th>
                        <th><a :class="{'selected-table-title': sortBy == 'total_balance'}" @click.prevent="sort('total_balance', $event)" href="#" v-html="$t('Total Balance')"></a>&nbsp;<span class="icon"><i :class="sortArrow('total_balance')"></i></span></th>
                        <th v-html="$t('Operation')"></th>
                    </tr>
                    </thead>
                    <thead v-else>
                    <tr>
                        <th v-html="$t('Coin')"></th>
                        <th v-html="$t('Available amount')"></th>
                        <th v-html="$t('Frozen amount')"></th>
                        <th v-html="$t('Total Balance')"></th>
                        <th v-html="$t('Operation')"></th>
                    </tr>
                    </thead>

                    <tbody v-if="(pageTokens.length > 0)">

                    <!--tr v-show="loading" style="vertical-align: middle; min-height: 150px;">
                        <td colspan="8"><ClipLoader :loading="loading" color="#666666"></ClipLoader></td>
                    </tr-->

                    <template v-for="row in pageTokens">
                        <tr :key="'values-' +row.code">

                            <td>
                                <div class="level">
                                    <div class="level-item has-text-centered"><img src="../../assets/account-bullet.png" align="center"></div>
                                    <div class="level-item level-column left-level-fix"><p>{{row.title}}</p><p style="color: #666;">{{row.code.toUpperCase()}}</p></div>
                                </div>
                            </td>
                            <td>{{row.active_balance}}</td>
                            <td>{{row.frozen_balance}}</td>
                            <td>{{row.total_balance}}</td>
                            <td><a @click="ToggleSubRow('deposit', row.code)">{{ 'Deposit' | translate }}</a><br><a @click="ToggleSubRow('withdraw', row.code)">{{ 'Withdrawal' | translate }}</a></td>
                        </tr>
                        <tr :id="'deposit-' +row.code" :style="{ 'display': ((openedSubRow == 'deposit-' +row.code) ? '' : 'none')}" :key="'deposit-' +row.code">
                            <td colspan="5" class="has-text-left">
                                <DepositWidget :ref="'deposit-' +row.code" :coin="row"></DepositWidget>
                            </td>
                        </tr>
                        <tr :id="'withdraw-' +row.code" :style="{ 'display': ((openedSubRow == 'withdraw-' +row.code) ? '' : 'none')}" :key="'withdraw-' +row.code">
                            <td colspan="5" class="has-text-left">
                                <WithdrawWidget :ref="'withdraw-' +row.code" :coin="row"></WithdrawWidget>
                            </td>
                        </tr>
                    </template>

                    </tbody>
                    <tbody v-else-if="((pageTokens.length == 0))">
                    <tr>
                        <td colspan="5" class="noTableContent">{{ 'Not-found-rows' | translate }}</td>
                    </tr>
                    </tbody>
                    <tbody v-else>
                    <tr>
                        <td colspan="5" class="noTableContent">Internal Error! Please try again in a few minutes. <span class="reloadErrorPage" @click="reloadPage"><i class="fas fa-sync-alt"></i></span></td>
                    </tr>
                    </tbody>

                </table>

                <br>

                <Paginator :current="currentPage" :countByPage="countByPage" :total="getTokenTotalCount(isHideZero)" :onSelectPage="OnPagerChange"></Paginator>
                <br>
                <DepositRecords></DepositRecords>

            </div>
        </div>
    </div>

</template>

<script>

	import Paginator from '@/components/ui/paginator';
	import *  as AccountsActions from '@/stores/modules/accounts/action.types';
	import *  as AccountsMutations from '@/stores/modules/accounts/mutation.types';
	import AccountWidget from '@/components/accounts/account.widget';

	import WithdrawWidget from '@/components/accounts/withdraw';
	import DepositWidget from '@/components/accounts/deposit';
	import DepositRecords from '@/components/accounts/deposit.records';

	export default {

		name: 'AccountsList',

		components: {
			WithdrawWidget, DepositWidget, Paginator, AccountWidget,DepositRecords
		},

		data()
		{
			return {
				currentPage: 1,
				countByPage: 10,
				spinShow: true,
				isHideZero: false,
				sortBy: "active_balance",
				sortOrder: "desc",
				filterByCoin: "",
                openedSubRow: "",
                latestSort: "",
			};
		},

		created()
		{
			let subscriber = this.$store.subscribe( ( mutation ) =>
			{
				if ( mutation.type == 'accounts/' + AccountsMutations.ACCOUNTS_MUTATION_LOADED ) {
					this.spinShow = false;
					subscriber();
				}
			} );

			let subscriberPreloadAccounts = this.$store.subscribe( ( mutation ) =>
            {
                if ( mutation.type == 'accounts/' + AccountsMutations.ACCOUNTS_MUTATION_LOAD_ACCOUNTS ) {
                    this.spinShow = false;
                }
            } );
		},

		computed: {

			getTokenTotalCount(){
				return this.$store.getters["accounts/getTokenTotalCount"];
			},

			getTokensByPage(){
				return this.$store.getters["accounts/getTokensByPage"];
			},

			getActiveAccount(){
				return this.$store.getters["accounts/getActiveAccount"];
			},

			pageTokens()
			{
                this.getTokenTotalCount(this.isHideZero)
			    let getTokenPage = this.getTokensByPage( this.currentPage, this.countByPage, this.isHideZero, this.sortBy, this.sortOrder, this.filterByCoin.trim() , (this.latestSort!=this.sortOrder))
                if(getTokenPage == -1) {
			        return 0;
                } else {
                    return getTokenPage;
                }
			},
		},

		methods: {

            hideZero() {
                this.isHideZero = !this.isHideZero;
            },

            reloadPage() {
                location.reload();
            },

			ToggleSubRow(type, id){

                if(this.openedSubRow != "")
                {
                    this.$refs[this.openedSubRow][0].OnHide()
                }               

				if(this.openedSubRow ==  type + "-"+id ){
					this.openedSubRow = "";
                } else {
                    this.openedSubRow = type + "-"+id;
                    this.$refs[this.openedSubRow][0].OnShow()
                }
            },

			OnPagerChange( page )
			{
                if(this.openedSubRow != ""){
                    this.$refs[this.openedSubRow][0].OnHide()
                    this.openedSubRow = "";
                }                    

				this.currentPage = page;
			},

			OnPagerPageSizeChange( page_size )
			{
				this.countByPage = page_size;
			},

			sort( id )
			{
                this.latestSort = this.sortOrder;

				if ( this.sortBy == id ) {
					if ( this.sortOrder == "asc" )
						this.sortOrder = "desc";
					else
						this.sortOrder = "asc";
				} else {
					this.sortBy = id;
					this.sortOrder = "desc";
				}
			},

			sortArrow( id )
			{
				if ( this.sortBy == id && this.sortOrder == "asc" ) {
					return "↑";
				}

				if ( this.sortBy == id && this.sortOrder == "desc" ) {
					return "↓";
				}

				return "";
			},
		}
	}

</script>

<style scoped>
    .noTableContent {
        text-align: center;
    }
    .reloadErrorPage {
        cursor: pointer;
        padding-left: 10px;
    }

    .search
    {
        background-image: url('../../assets/search.png');
        width: 33px;
        height: 33px;
    }
</style>