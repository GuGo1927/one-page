import Vue from 'vue';
import ServiceConfig from './config';

export default (name, email, password) => {
	
	var o = { name: name, email: email, password: password };
	return Vue.Api.Post(ServiceConfig.service + "/users", o);
	
};
