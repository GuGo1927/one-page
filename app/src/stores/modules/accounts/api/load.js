import ApiLoadCurrency from './load.currency';
import ApiLoadTradeAccount from './load.trade.accounts';
import ApiLoadAccount from './load.accounts';

export default ( trade_account ) => {

	return new Promise (( resolve, reject ) => {

		let results = { currencies: {}, accounts: [], balances: [] };

		ApiLoadCurrency ().then (function ( currencies ) {

			console.log("CURRENCIES LOADED");

			let c;

			for ( var i = 0; i < currencies.data.length; i++ ) {
				c = currencies.data[ i ];
				results.currencies[ c.code ] = c;
			}

			ApiLoadTradeAccount ().then (( trade_accounts ) => {

				console.log("TRADE ACCOUNTS LOADED");

				results.accounts = trade_accounts.data.rows;

				/*for ( var i = 0; i < results.accounts.length; i++ ) {
					results.accounts[ i ].usd_balance = 0;
					results.accounts[ i ].btc_balance = 0;
				}*/

				ApiLoadAccount (results.accounts[ 0 ].id).then (( accounts ) => {

					console.log("ACCOUNTS LOADED");
					console.log(results);

					let item;
					for ( var key in results.currencies ) {

						item = {
							code: results.currencies[ key ].code
							, currency_id: results.currencies[ key ].id
							, title: results.currencies[ key ].name
							, active_balance: 0
							, frozen_balance: 0
							, total_balance: 0
							, depositAddress: ""
							, id: 0
							, is_active: true
						};

						results.balances.push(item);
					}

					console.log("RESULT");
					console.log(results);

					let a, idx;
					if(accounts.data.rows != null)
					{
						for ( var i = 0; i < accounts.data.rows.length; i++ ) {

							a    = accounts.data.rows[ i ];
							idx = results.balances.findIndex(acc => acc.currency_id == a.currency_id);

							results.balances[ idx ].active_balance = a.active_balance;
							results.balances[ idx ].frozen_balance = a.hold_balance;
							results.balances[ idx ].id = a.id;
							results.balances[ idx ].total_balance = a.active_balance + a.hold_balance;
							results.balances[ idx ].is_active = a.is_active;
						}
					}

					//results.balances.sort((a, b) => { a.title.localeCompare( b.title ) });

					console.log("RESULT");
					console.log(results);

					resolve (results);

				}, ( err_account ) => {
					console.log("ERROR LOAD ACCOUNTS");
					reject (err_account);
				});

			}, ( err_trade_account ) => {
				console.log("ERROR LOAD TRADE ACCOUNTS");
				reject (err_trade_account);
			});

		}, ( err_currency ) => {
			console.log("ERROR LOAD CURRENCIES");
			reject (err_currency);
		});
	});
};
