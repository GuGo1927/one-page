export default {
	
	service: process.env.TRADE_SERVICE_URL,
	paymentService: process.env.PAYMENT_SERVICE_URL,
	
}
