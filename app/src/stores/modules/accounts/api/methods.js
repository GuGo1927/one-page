import API_Deposit from './deposit';
import API_Load from './load';
import API_LoadAccounts from './load.accounts';
import API_Transactions from './load.transactions';
import API_Withdraw from './withdraw';

export const DepositRequest   = API_Deposit;
export const LoadRequest      = API_Load;
export const LoadAccounts      = API_LoadAccounts;
export const WithdrawdRequest = API_Withdraw;
export const TransactionsRequest = API_Transactions;