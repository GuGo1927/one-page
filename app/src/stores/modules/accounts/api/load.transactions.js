import Vue from 'vue';
import ServiceConfig from './config';

export default () => {
    return Vue.Api.Get(ServiceConfig.paymentService +'/transactions');
};
