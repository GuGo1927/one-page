import Vue from 'vue';
import ServiceConfig from './config';

export default (trade_account) => {
	return Vue.Api.Get(ServiceConfig.service +'/account', { filter: 'trading_account_id|eq|'+trade_account });
};
