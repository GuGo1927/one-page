export default {

	accounts:  [],
	withdraws: [],
	deposits:  [],
	transactions: [],
	
	currencies:  [],
	balances:  [],
	
	active_account: -1,
    error_account: false,
}