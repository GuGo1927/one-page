import Vue from 'vue';
import * as types from './action.types';
import * as mutations from './mutation.types';
import ServiceConfig from './api/config';

import { LoadRequest, LoadAccounts, TransactionsRequest } from './api/methods';

export default {

	[types.GET_WALLET](context, payload){
		const currencyCode = payload.code;
		
		Vue.Api.Get(ServiceConfig.paymentService + "/"+currencyCode+"/accounts").then((response) => {

			if ( response.status == 200) {
				const result = response.data;

				if(result.data.length > 0)
				{
					context.commit( mutations.WALLET_RECEIVED_SUCCESS, { address : result.data[0].address, code: currencyCode } );
					return 
				} else {
					context.commit( mutations.WALLET_RECEIVED_SUCCESS, { address : "none", code: currencyCode  } );
					return 
				}
			} else {
				context.commit( mutations.WALLET_RECEIVED_FAIL);
			}			

		}, 
		(e)        => context.commit( mutations.WALLET_RECEIVED_FAIL, e) )
		.catch((e) => context.commit( mutations.WALLET_RECEIVED_FAIL, e) );
	},

	["test"](context){
		context.commit("test", { balance: 10});
	},

	[types.CREATE_WALLET](context, payload){
		const currencyCode = payload.code;

		Vue.Api.Post(ServiceConfig.paymentService + "/"+currencyCode+"/accounts/new").then((response) => {

			if ( response.status == 200) {
				const result = response.data;
				context.commit( mutations.WALLET_CREATED_SUCCESS, { address : result.address, code: currencyCode } );
				return;
			} else {
				context.commit( mutations.WALLET_CREATED_FAIL, response);
			}			

		}, 
		(e)        => context.commit( mutations.WALLET_CREATED_FAIL, e) )
		.catch((e) => context.commit( mutations.WALLET_CREATED_FAIL, e) );
	},
	
	[types.ACCOUNTS_ACTION_DEPOSIT]( context, payload )
	{
		context.commit( mutations.ACCOUNTS_MUTATION_DEPOSIT_ADDED, payload );
	},
	
	[types.ACCOUNTS_ACTION_LOAD_DEPOSITS](){},
	
	[types.ACCOUNTS_ACTION_LOAD_WITHDRAWS](){},

	/**
	 * Initial load: balances, trading accounts, currencies
	 * @param context
	 */
	[types.ACCOUNTS_ACTION_LOAD]( context )
	{
		LoadRequest().then((results) => {
            context.commit( mutations.ACCOUNTS_MUTATION_LOADED, results );

		}, (err) => {
            context.commit( mutations.ACCOUNTS_MUTATION_LOADED, err );
			console.error(err);
		});
	},
    [types.ACCOUNTS_TRANSACTIONS]( context, payload )
    {
        Vue.Api.Get(ServiceConfig.paymentService + "/btc/transactions").then((response) => {
				if((response.data.data != [] && response.data.data != undefined)) {
                    context.commit( mutations.ACCOUNTS_MUTATION_LOAD_TRANSACTIONS, response.data.data );
				} else {
                    context.commit( mutations.ACCOUNTS_MUTATION_LOAD_TRANSACTIONS, [] );
				}
            },(e) => {
                console.log(e)
            }).catch((e) => {
	            console.log(e)
            })
    },

	/**
	 * Load balances for account
	 * @param context
	 * @param payload
	 */
	[types.ACCOUNTS_ACTION_LOAD_ACCOUNTS]( context, payload )
	{
		const acc_idx = context.state.accounts.findIndex( acc => acc.id == payload.account );

		if(acc_idx > -1)
		{
			LoadAccounts(context.state.accounts[acc_idx].id).then((accounts) => {

				let item;
				let balances = [];

				for ( var key in context.state.currencies ) {

					item = {
						code: context.state.currencies[ key ].code
						, title: context.state.currencies[ key ].name
						, active_balance: 0
						, frozen_balance: 0
						, total_balance: 0
						, currency_id: context.state.currencies[ key ].id
						, is_active: true
					};

					balances.push(item);
				}

				let a, idx;
				for ( var i = 0; i < accounts.data.rows.length; i++ ) {

					a    = accounts.data.rows[ i ];
					idx = balances.findIndex(acc => acc.id == a.currency_id);

					balances[ idx ].active_balance = 0;
					balances[ idx ].frozen_balance = 0;
					balances[ idx ].total_balance = 0;
					balances[ idx ].is_active = a.is_active;
				}

				balances.sort((a, b) => { a.title.localeCompare( b.title ) });
				context.commit( mutations.ACCOUNTS_MUTATION_LOAD_ACCOUNTS, { balances: balances } );

			}, (err) => {
				console.error(err);
			});
		}
	},

	/**
	 * Select other account and load balances
	 * @param context
	 * @param payload
	 */
	[types.ACCOUNTS_SELECT_ACTIVE]( context, payload )
	{
		const acc_idx = context.state.accounts.findIndex( acc => acc.id == payload.account );

		if(acc_idx > -1)
		{
			context.commit( mutations.ACCOUNTS_SELECTED, { account: acc_idx } );
			context.dispatch(types.ACCOUNTS_ACTION_LOAD_ACCOUNTS, { account: payload.account });
		}
	},
	
	[types.ACCOUNTS_ACTION_WITHDRAW]( context, payload )
	{
		context.commit( mutations.ACCOUNTS_MUTATION_WITHDRAW_FINISHED, payload );
	},
	
}
