
export default {

	getAccountById : (state) => (id) => {
		return state.accounts.find(acc => acc.id == id);
	},

	getAccoutInfo: (state) => (currencyCode) => {		
		const index = state.balances.findIndex(balance => balance.code.toLowerCase() == currencyCode.toLowerCase());

		if(index < -1){
			return null;
		}		

		return state.balances[index];
	},
	
	getActiveAccount: (state) => {
		
		if(state.active_account == -1)
			return {
				id: "---",
				number: "---",
				balances: []
			};

		return state.accounts[state.active_account];
	},

	getAccountsWithoutActive: (state) => {
		
		let accounts = state.accounts.slice();
		accounts.splice(state.active_account, 1);
		
		return accounts;
	},

	getAllBalances: (state) => {
		return state.balances;
	},

	getTokensByPage: (state, getters) => (page, countByPage, isHideZero, sortBy, sortOrder, searchString ,isChangedSort) => {
		let errorAccount = state.error_account;
        if(errorAccount) {
            return -1;
        } else {
            countByPage = parseInt(countByPage);
            isChangedSort = isChangedSort || false;

            console.log("page: "+page+"; countByPage: "+countByPage+"; isHideZero: "+isHideZero+"; sortBy: "+sortBy+"; sortOrder: "+sortOrder+"; searchString: "+searchString);

            if(countByPage <= 0)
            {
                return [];
            }

            let startFrom = page * countByPage - countByPage;
            //if(page > 1) startFrom--;

            let end = startFrom + countByPage;
            //if(page > 1) end--;

            let filtered =  [...state.balances] || [];
            let isFiltered = false;

            console.log("Total balances: "+filtered.length);
            console.log(filtered);

            if(isHideZero === true){
                filtered = filtered.filter(item => item.active_balance > 0);
                console.log("Is Hide zerro: "+filtered.length);
            }

            if(searchString != undefined && searchString != "")
            {
                isFiltered = true;
                searchString = searchString.toLowerCase();

                let tmp  = filtered.filter(item => { const idx = item.code.toLowerCase().indexOf(searchString); if(idx > -1){ item.index = idx; return true; } return false; });
                let tmp2 = filtered.filter(item => { const idx = item.title.toLowerCase().indexOf(searchString); if(idx > -1){ item.index = idx; return true; } return false; });

                filtered = tmp.filter( aa => ! tmp2.find ( bb => aa['code'] === bb['code']) ).concat(tmp2);
                filtered.sort((a,b) => (a.index > b.index) ? 1 : ((b.index > a.index) ? -1 : 0) );

                console.log("Is Hide zerro: "+filtered.length);
            }

            if(isFiltered == false)
            {
                console.log(filtered);
                filtered.sort((a,b) => (a[sortBy] > b[sortBy]) ? 1 : ((b[sortBy] > a[sortBy]) ? -1 : 0) )
                if(sortOrder != "asc") filtered.reverse();
                console.log(filtered);

                console.log("Sort without filtered: "+filtered.length);
            }

            if(filtered.length == 0)
                return [];

            if(end > filtered.length){
                end  = filtered.length;
            }

            console.log("Slice from: "+startFrom+" to "+end);
            console.log(filtered.slice(startFrom, end));

            return filtered.slice(startFrom, end);
        }
	},

	getTokenTotalCount: (state, getters) => (filter) => {
        let filtered = [...state.balances] || [];
        if(filter === true) {
            filtered = filtered.filter(item => item.active_balance > 0);
            return filtered.length;
        } else {
            return state.balances.length;
        }

	},
    getDeposits: (state, getters) => {
        let transactions = [...state.transactions] || [];
        return transactions
    }

}