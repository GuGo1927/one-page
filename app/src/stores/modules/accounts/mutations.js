import * as mutations from './mutation.types';

export default {
	
	["test"](state, payload){
		let balances = [...state.balances];
		balances[0].active_balance = 10;
		state.balances = balances;
	},

	[mutations.ACCOUNTS_MUTATION_DEPOSIT_ADDED]( state, payload )
	{
		const amount = parseFloat( payload.amount.toFixed( 8 ) );
		const account_id = payload.account_id;
		const token = payload.token;
		
		const account_idx = state.accounts.findIndex( acc => acc.id == account_id );

		if ( amount <= 0 || account_idx == -1 ) {
			return;
		}
		
		let token_idx = state.accounts[ account_idx ].balances.findIndex( tok => { return tok.code == token; } );


		if ( token_idx == -1 ) {
			return;
		}
		
		state.accounts[ account_idx ].balances[ token_idx ].active_balance += amount;
	},
	
	[mutations.ACCOUNTS_SELECTED]( state, payload )
	{
		state.active_account = payload.account;
	},
    [mutations.ACCOUNTS_MUTATION_LOAD_TRANSACTIONS]( state, payload )
	{
		state.transactions = payload;
    },

	/**
	 * Update initial data : currencies, trading accounts, balances, active trading account
	 * @param state
	 * @param payload
	 */
	[mutations.ACCOUNTS_MUTATION_LOADED]( state, payload )
	{
		if(payload.status == 500) {
            state.error_account = true;
        } else {
            state.accounts   = payload.accounts;
            state.currencies = payload.currencies;
            state.balances   = payload.balances;
            state.active_account = 0;
            state.error_account = false;
		}
	},

	/**
	 * Update Balances when choice new active trade account
	 * @param state
	 * @param payload
	 */
	[mutations.ACCOUNTS_MUTATION_LOAD_ACCOUNTS]( state, payload )
	{
		state.balances       = payload.balances;
		state.active_account = payload.account;
	},

	[mutations.WALLET_RECEIVED_SUCCESS](state, payload)
	{
		const balance_idx = state.balances.findIndex( balance => balance.code.toLowerCase() == payload.code);
		let balance = state.balances[balance_idx];
		balance.depositAddress = payload.address;		

		state.balances.splice(balance_idx, 1, balance);		
	},
	
	[mutations.WALLET_RECEIVED_FAIL](state, payload)
	{
		console.log(payload);
	},

	[mutations.WALLET_CREATED_SUCCESS](state, payload)
	{
		const balance_idx = state.balances.findIndex( balance => balance.code.toLowerCase() == payload.code);
		let balance = state.balances[balance_idx];
		balance.depositAddress = payload.address;	
		
		console.log(balance);

		state.balances.splice(balance_idx, 1, balance);		
	},
	
	[mutations.WALLET_CREATED_FAIL](state, payload)
	{
		console.log(payload);
	},
	
	[mutations.ACCOUNTS_MUTATION_WITHDRAW_FINISHED]( state, payload )
	{
		console.log(payload);
		console.log(payload.amount);
		
		const amount = parseFloat( payload.amount.toFixed( 8 ) );
		const account_id = payload.account_id;
		const token = payload.token;
		
		const account_idx = state.accounts.findIndex( acc => acc.id == account_id );
		
		if ( amount <= 0 || account_idx == -1 ) {
			return;
		}
		
		const token_idx = state.accounts[ account_idx ].balances.findIndex( tok => tok.code == token );
		
		if ( token_idx == -1 ) {
			return;
		}
		
		state.accounts[ account_idx ].balances[ token_idx ].active_balance -= amount;
	},
	
}