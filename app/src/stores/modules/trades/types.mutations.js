/**
 * All Trades loaded
 * @type {string}
 */
export const M_TRADES_LOAD_COMPLETE = "M_TRADES_LOAD_COMPLETE";
