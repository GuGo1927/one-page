import * as mutation_types from './types.mutations';

export default {

    [mutation_types.M_TRADES_LOAD_COMPLETE] ( state, payload ) {
        state.trades = payload.trades;
    }
};

