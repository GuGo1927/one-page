import Vue from 'vue';
import ServiceConfig from './config';

export default (trade_market) => {
    return Vue.Api.Get(ServiceConfig.service + '/market');
};
