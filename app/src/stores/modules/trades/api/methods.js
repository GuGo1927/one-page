import API_Market from './load.markets';
import API_Currency from './load.currency';

export const LoadMarket = API_Market;
export const LoadCurrency = API_Currency;
