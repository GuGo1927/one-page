import Vue from 'vue';
import ServiceConfig from './config';

export default (trade_currency) => {
    return Vue.Api.Get(ServiceConfig.service + '/currency');
};
