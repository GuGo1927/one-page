import * as types from './types.actions';
import * as mutation_types from './types.mutations';
import { LoadMarket, LoadCurrency} from './api/methods';

export default {

    [types.A_LOAD_TRADES] ( context, payload ) {
        LoadMarket().then ((response) => {
            if(response.status == 200 && response.data ) {
                let trades = [];
                let currency = [];
                LoadCurrency().then(function (currencies) {
                    if(currencies.status == 200 && currencies.data) {
                        currency = currencies.data;
                        for(var key in response.data.rows ) {
                            if(response.data.rows[key]['first_currency_id'] == 1) {
                                let data_trades = [];
                                for(var cur in currency) {
                                    if(currency[cur]['id'] == response.data.rows[key]['second_currency_id']) {
                                        data_trades = response.data.rows[key];
                                        data_trades['name'] = currency[cur]['name'];
                                    }
                                }
                                trades.push(data_trades);
                            }
                        }
                        context.commit (mutation_types.M_TRADES_LOAD_COMPLETE, { trades: trades });

                    } else {
                        context.commit (mutation_types.M_TRADES_LOAD_COMPLETE, { trades: [] });
                    }

                }, ( error ) => {
                    context.commit (mutation_types.M_TRADES_LOAD_COMPLETE, { trades: [] });
                }).catch(( error ) => {
                    context.commit (mutation_types.M_TRADES_LOAD_COMPLETE, { trades: [] });
                });


            } else {
                context.commit (mutation_types.M_TRADES_LOAD_COMPLETE, { trades: [] });
            }

        }, ( error ) => {
            context.commit (mutation_types.M_TRADES_LOAD_COMPLETE, { trades: [] });
        }).catch(( error ) => {
            context.commit (mutation_types.M_TRADES_LOAD_COMPLETE, { trades: [] });
        })

    },
}