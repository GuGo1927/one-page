import * as mutation_types from './types.mutations';

export default {

	[mutation_types.M_COINS_LOAD_COMPLETE] ( state, payload ) {
		state.coins = payload.coins;
	},

	[mutation_types.M_MARKET_LOAD_COMPLETE] ( state, payload ) {
		state.markets = payload.markets;
	},
};

