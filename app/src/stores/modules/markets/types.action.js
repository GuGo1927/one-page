/**
 * Load All Coins
 * @type {string}
 */
export const A_LOAD_COINS = "A_LOAD_COINS";

/**
 * Load All Markets
 * @type {string}
 */
export const A_LOAD_MARKETS = "A_LOAD_MARKETS";

/**
 * Load All Coins and All Markets
 * @type {string}
 */
export const A_LOAD_COINS_AND_MARKETS = "A_LOAD_COINS_AND_MARKETS";