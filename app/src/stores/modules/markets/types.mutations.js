/**
 * All Coins loaded
 * @type {string}
 */
export const M_COINS_LOAD_COMPLETE = "M_COINS_LOAD_COMPLETE";

/**
 * All Markets loaded
 * @type {string}
 */
export const M_MARKET_LOAD_COMPLETE = "M_MARKET_LOAD_COMPLETE";