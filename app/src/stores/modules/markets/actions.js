import * as types from './types.action';
import * as mutation_types from './types.mutations';
import Vue from 'vue';

export default {

	[types.A_LOAD_COINS] ( context, payload ) {

		Vue.Api.Get (process.env.BACKEND_URL + '/coins.list').then (( response ) => {

			if ( response.status == 200 && response.data ) {

				let results = [];
				let item;
				for(var i = 0; i < response.data.coins.length; i++)
				{
					item = response.data.coins[i];

					item.percent_change_24h /= 100;
					item.percent_change_1h /= 100;
					item.percent_change_7d /= 100;

					item.market_cap_usd_formated = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' , minimumFractionDigits: 0, maximumFractionDigits: 0}).format(item.market_cap_usd);
					item.price_usd_formated = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' , minimumFractionDigits: 2, maximumFractionDigits: 6}).format(item.price_usd);
					item.price_btc_formated = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'BTC' , minimumFractionDigits: 2, maximumFractionDigits: 8}).format(item.price_btc);
					item.volume_24h_usd_formated = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' , minimumFractionDigits: 0, maximumFractionDigits: 0}).format(item.volume_24h_usd);
					item.percent_change_24h_formated = new Intl.NumberFormat('en-US', { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2}).format(item.percent_change_24h);
					item.percent_change_1h_formated = new Intl.NumberFormat('en-US', { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2}).format(item.percent_change_1h);
					item.percent_change_7d_formated = new Intl.NumberFormat('en-US', { style: 'percent', minimumFractionDigits: 2, maximumFractionDigits: 2}).format(item.percent_change_7d);

					if(item.percent_change_24h > 0){
						item.percent_change_24h_class = 'up_change';
					} else if(item.percent_change_24h < 0){
						item.percent_change_24h_class = 'down_change';
					} else {
						item.percent_change_24h_class = 'net';
					}

					if(item.percent_change_1h > 0){
						item.percent_change_1h_class = 'up_change';
					} else if(item.percent_change_1h < 0){
						item.percent_change_1h_class = 'down_change';
					} else {
						item.percent_change_1h_class = 'net';
					}

					if(item.percent_change_7d > 0){
						item.percent_change_7d_class = 'up_change';
					} else if(item.percent_change_7d < 0){
						item.percent_change_7d_class = 'down_change';
					} else {
						item.percent_change_7d_class = 'net';
					}

					item.available_supply_formated = (new Intl.NumberFormat('en-EN', { minimumFractionDigits: 0, maximumFractionDigits: 0}).format(item.available_supply)) + ' ' + item.symbol;
					item.max_supply_formated = (item.max_supply != null && item.max_supply > 0) ? (new Intl.NumberFormat('en-EN', { minimumFractionDigits: 0, maximumFractionDigits: 0}).format(item.max_supply)) + ' ' + item.symbol : "---";
					item.price_btc_formated = (new Intl.NumberFormat('en-EN', { minimumFractionDigits: 0, maximumFractionDigits: 8}).format(item.price_btc));

					results.push(item);
				}

				context.commit (mutation_types.M_COINS_LOAD_COMPLETE, { coins: results });
			} else {
				context.commit (mutation_types.M_COINS_LOAD_COMPLETE, { coins: [] });
			}

		}, ( error ) => {
			context.commit (mutation_types.M_COINS_LOAD_COMPLETE, { coins: [] });
		}).catch (( error ) => {
			context.commit (mutation_types.M_COINS_LOAD_COMPLETE, { coins: [] });
		});

	},

	[types.A_LOAD_COINS_AND_MARKETS](context, payload)
	{

	},

	[types.A_LOAD_MARKETS] ( context, payload ) {

		Vue.Api.Get (process.env.BACKEND_URL + '/markets.list').then (( response ) => {

			if ( response.status == 200 && response.data ) {

				var obj = {};
				for(var i = 0; i < response.data.markets.length; i++)
				{
					obj[response.data.markets[i].market_id] = response.data.markets[i];
				}

				context.commit (mutation_types.M_MARKET_LOAD_COMPLETE, { markets: obj });
			} else {
				context.commit (mutation_types.M_MARKET_LOAD_COMPLETE, { markets: {} });
			}

		}, ( error ) => {
			context.commit (mutation_types.M_MARKET_LOAD_COMPLETE, { markets: {} });
		}).catch (( error ) => {
			context.commit (mutation_types.M_MARKET_LOAD_COMPLETE, { markets: {} });
		});

	},
}