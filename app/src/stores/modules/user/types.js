
/** Authorization action **/
export const A_LOGIN = "A_LOGIN";

export const LOGIN_FROM_COOKIE = "LOGIN_FROM_COOKIE";

/** Authorization success event **/
export const M_LOGIN_SUCCESS = "M_LOGIN_SUCCESS";

/** Authorization fail event **/
export const M_LOGIN_FAIL = "M_LOGIN_FAIL";

/** Resend Confirmation action **/
export const A_RESEND_CONFIRMATION = "A_RESEND_CONFIRMATION";

/** Registration action **/
export const A_UPDATE_PASSWORD = "A_UPDATE_PASSWORD";

/** Registration action **/
export const A_REGISTER = "A_REGISTER";

/** Registration success event **/
export const M_REGISTER_SUCCESS = "M_REGISTER_SUCCESS";

/** Registration fail event **/
export const M_REGISTER_FAIL = "M_REGISTER_FAIL";

/** Send restore password request action **/
export const A_RESTORE_PASSWORD = "A_RESTORE_PASSWORD";

export const NEW_PASSWORD = "NEW_PASSWORD";
export const CREATE_SECURE_KEY_2FA = "CREATE_SECURE_KEY_2FA";
export const ENABLE_2FA = "ENABLE_2FA";
export const CHECK_2FA = "CHECK_2FA";
export const DISABLE_2FA = "DISABLE_2FA";
export const CHANGE_LANG = "CHANGE_LANG";

/** Restore password request success event **/
export const M_RESTORE_PASSWORD_SUCCESS = "M_RESTORE_PASSWORD_SUCCESS";

/** Restore password request fail event **/
export const M_RESTORE_PASSWORD_FAIL = "M_RESTORE_PASSWORD_FAIL";

/** Logout action **/
export const A_LOGOUT = "A_LOGOUT";

/** Confirm action **/
export const A_CONFIRM_REGISTRATION = "A_CONFIRM_REGISTRATION";

/** Logout success event **/
export const M_LOGOUT_SUCCESS = "M_LOGOUT_SUCCESS";

/** Confirm by 2FA **/
export const A_2FA_CONFIRM = "A_2FA_CONFIRM";

/** Confirm by 2FA success event **/
export const M_2FA_CONFIRM_SUCCESS = "M_2FA_CONFIRM_SUCCESS";

/** Confirm by 2FA fail event **/
export const M_2FA_CONFIRM_FAIL = "M_2FA_CONFIRM_FAIL";
