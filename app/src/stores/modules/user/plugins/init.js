import Vue from 'vue';

const initPlugin = store => {

	const GetCookie = (name) => {
		var value = "; " + document.cookie;
		var parts = value.split("; " + name + "=");
		if (parts.length == 2) return parts.pop().split(";").shift();
	}

	let authToken = GetCookie ('token') || false;

	console.log("User Init Plugin");
	console.log(authToken);

	if(authToken != "" && authToken != false)
	{
		store.commit('user/LOGIN_INIT_DATA', { token: authToken });

	}
}

export default initPlugin;