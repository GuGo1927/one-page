/** Authorization success event **/
export const M_LOGIN_SUCCESS = "M_LOGIN_SUCCESS";

/** Authorization fail event **/
export const M_LOGIN_FAIL = "M_LOGIN_FAIL";

export const LOGIN_INIT_DATA = "LOGIN_INIT_DATA";

/** Resend Confirmation events **/
export const M_RESEND_CONFIRMATION_SUCCESS = "M_RESEND_CONFIRMATION_SUCCESS";
export const M_RESEND_CONFIRMATION_FAIL    = "M_RESEND_CONFIRMATION_FAIL";

/** Authorization success event **/
export const M_CONFIRM_REGISTRATION_SUCCESS = "M_CONFIRM_REGISTRATION_SUCCESS";

/** Authorization fail event **/
export const M_CONFIRM_REGISTRATION_FAIL = "M_CONFIRM_REGISTRATION_FAIL";

/** Registration success event **/
export const M_REGISTER_SUCCESS = "M_REGISTER_SUCCESS";

/** Registration fail event **/
export const M_REGISTER_FAIL = "M_REGISTER_FAIL";

/** Restore password request success event **/
export const M_RESTORE_PASSWORD_SUCCESS = "M_RESTORE_PASSWORD_SUCCESS";

export const NEW_PASSWORD_SUCCESS = "NEW_PASSWORD_SUCCESS";
export const NEW_PASSWORD_FAIL = "NEW_PASSWORD_FAIL";

export const CREATE_SECRET_2FA_SUCCESS = "CREATE_SECRET_2FA_SUCCESS";
export const CREATE_SECRET_2FA_FAIL = "CREATE_SECRET_2FA_FAIL";

export const ENABLE_2FA_SUCCESS = "ENABLE_2FA_SUCCESS";
export const ENABLE_2FA_FAIL = "ENABLE_2FA_FAIL";

export const CHECK_2FA_SUCCESS = "CHECK_2FA_SUCCESS";
export const CHECK_2FA_FAIL = "CHECK_2FA_FAIL";

export const DISABLE_2FA_SUCCESS = "DISABLE_2FA_SUCCESS";
export const DISABLE_2FA_FAIL = "DISABLE_2FA_FAIL";

/** Restore password request fail event **/
export const M_RESTORE_PASSWORD_FAIL = "M_RESTORE_PASSWORD_FAIL";

/** Logout success event **/
export const M_LOGOUT_SUCCESS = "M_LOGOUT_SUCCESS";

/** Confirm by 2FA success event **/
export const M_2FA_CONFIRM_SUCCESS = "M_2FA_CONFIRM_SUCCESS";

/** Confirm by 2FA fail event **/
export const M_2FA_CONFIRM_FAIL = "M_2FA_CONFIRM_FAIL";