import * as mutation_types from './mutation_types';

export default {
	
	/**
	 * Authorization success Handler
	 * @param state
	 * @param payload
	 */
	[mutation_types.M_LOGIN_SUCCESS]( state, payload )
	{
		state.isAuthenticated = true;
		state.token = payload.token;
		state.token_expired = payload.expired;
		state.user = payload.user;
		state.isValidateToken = true;
	},

	
	[mutation_types.M_CONFIRM_REGISTRATION_SUCCESS]( state, payload )
	{
	},

	[mutation_types.CREATE_SECRET_2FA_FAIL]( state, payload )
	{
	},

	[mutation_types.ENABLE_2FA_SUCCESS]( state, payload )
	{
		if(state.connection['2fa'] == null)
			state.connection['2fa'] = { url: "", secret: "", status: false, isInitialized: false };

		state.connection['2fa'].status = true;
		state.connection['2fa'].isInitialized = true;
	},

	[mutation_types.ENABLE_2FA_FAIL]( state, payload )
	{
	},

	[mutation_types.DISABLE_2FA_SUCCESS]( state, payload )
	{
		if(state.connection['2fa'] == null)
			state.connection['2fa'] = { url: "", secret: "", status: false, isInitialized: false };

		state.connection['2fa'].status = false;
		state.connection['2fa'].isInitialized = true;
	},

	[mutation_types.DISABLE_2FA_FAIL]( state, payload )
	{
	},

	[mutation_types.CREATE_SECRET_2FA_SUCCESS]( state, payload )
	{
		if(state.connection['2fa'] == null)
			state.connection['2fa'] = { url: "", secret: "", status: false, isInitialized: false };

		state.connection['2fa'].url = payload.url;
		state.connection['2fa'].secret = payload.secret;
	},

	[mutation_types.CHECK_2FA_SUCCESS]( state, payload )
	{
		if(state.connection['2fa'] == null)
			state.connection['2fa'] = { url: "", secret: "", status: false, isInitialized: false };

		state.connection['2fa'].status = payload.enabled;
		state.connection['2fa'].isInitialized = true;
	},

	[mutation_types.NEW_PASSWORD_FAIL]( state, payload )
	{
	},

	[mutation_types.NEW_PASSWORD_SUCCESS]( state, payload )
	{
	},

	[mutation_types.LOGIN_INIT_DATA]( state, payload )
	{
		state.isAuthenticated = true;
		state.token = payload.token;
		state.user = payload.user;
		state.isValidateToken = true;
	},
	
	[mutation_types.M_CONFIRM_REGISTRATION_FAIL]( state, payload )
	{
	},

	[mutation_types.M_RESEND_CONFIRMATION_SUCCESS]( state, payload )
	{
	},

	[mutation_types.M_RESEND_CONFIRMATION_FAIL]( state, payload )
	{
	},
	
	/**
	 * Authorization fail Handler
	 * @param state
	 * @param payload
	 */
	[mutation_types.M_LOGIN_FAIL]( state, payload )
	{
		state.isAuthenticated = false;
		state.user = null;
	},
	
	/**
	 * Registration is successfully
	 * @param state
	 * @param payload
	 */
	[mutation_types.M_REGISTER_SUCCESS]( state, payload )
	{
		state.isAuthenticated = false;
		state.user = null;
	},
	
	/**
	 * Registration is failed
	 * @param state
	 * @param payload
	 */
	[mutation_types.M_REGISTER_FAIL]( state, payload )
	{
		state.isAuthenticated = false;
		state.user = null;
	},
	
	/**
	 * Request for restore password is accepted
	 * @param state
	 * @param payload
	 */
	[mutation_types.M_RESTORE_PASSWORD_SUCCESS]( state, payload )
	{
		state.isAuthenticated = false;
		state.user = null;
	},
	
	/**
	 * Request for restore password is declined
	 * @param state
	 * @param payload
	 */
	[mutation_types.M_RESTORE_PASSWORD_FAIL]( state, payload )
	{
		state.isAuthenticated = false;
		state.user = null;
	},
	
	/**
	 * Logout user
	 * @param state
	 * @param payload
	 */
	[mutation_types.M_LOGOUT_SUCCESS]( state )
	{
		state.isAuthenticated = false;
		state.user = null;
		state.isValidateToken = false;
		state.token = "";
	},
	
	/**
	 * Request for restore password is accepted
	 * @param state
	 * @param payload
	 */
	[mutation_types.M_2FA_CONFIRM_SUCCESS]( state, payload )
	{
		state.isAuthenticated = false;
		state.user = null;
	},
	
	/**
	 * Request for restore password is declined
	 * @param state
	 * @param payload
	 */
	[mutation_types.M_2FA_CONFIRM_FAIL]( state, payload )
	{
		state.isAuthenticated = false;
		state.user = null;
	},
	
}
