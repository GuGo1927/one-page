import Vue from 'vue';
import ServiceConfig from './config';

export default (token, password, code2fa) => {

  var data = { token: token, password: password };

  if(code2fa != undefined && code2fa != ""){
	  data['2fa_token'] = code2fa;
  }

  return Vue.Api.Post(ServiceConfig.service + "/password/forgot/new", data);
  
};
