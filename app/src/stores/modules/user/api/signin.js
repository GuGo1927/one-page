import Vue from 'vue';
import ServiceConfig from './config';

export default (email, password, code) => {
  
  var o = { email: email, password: password };

  var options = {};
  if(code != undefined)
    options['headers'] = { 'X-Auth-2fa': code };

  return Vue.Api.Post(ServiceConfig.service + "/login", o, options);
  
};
