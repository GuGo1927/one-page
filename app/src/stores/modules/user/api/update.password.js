import Vue from 'vue';
import ServiceConfig from './config';

export default (id, password) => {
	
	var o = { password: password };
	return Vue.Api.Put(ServiceConfig.service + "/users/"+parseInt(id), o);
	
};
