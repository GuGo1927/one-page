import Vue from 'vue';
import ServiceConfig from './config';

export default (lang) => {

  return Vue.Api.Post(ServiceConfig.service + "/user/lang", { lang: lang });
  
};
