import Vue from 'vue';
import ServiceConfig from './config';

export default (name, email, password, lang) => {
	
	var o = { username: name, email: email, password: password, country: "UA", lang: lang };
	return Vue.Api.Post(ServiceConfig.service + "/registration", o);
  
};
