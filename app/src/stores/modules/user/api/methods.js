import API_SignInRequest from './signin';
import API_SignUpRequest from './register';
import API_LogOutRequest from './logout';
import API_RestorePasswordRequest from './restore.password';
import API_NewPasswordRequest from './new.password';
import API_UpdatePasswordRequest from './update.password';
import API_ConfirmRequest from './confirm.registration';
import API_ConfirmResendRequest from './resend.confirmation';
import API_CreateSecret2FARequest from './create.secret.2fa';
import API_Enable2FARequest from './enable.2fa';
import API_Disable2FARequest from './disable.2fa';
import API_Check2FARequest from './check.2fa';
import API_ChangeLangRequest from './change.language';

export const SignInRequest          = API_SignInRequest;
export const SignUpRequest          = API_SignUpRequest;
export const LogOutRequest          = API_LogOutRequest;
export const RestorePasswordRequest = API_RestorePasswordRequest;
export const NewPasswordRequest = API_NewPasswordRequest;
export const UpdatePasswordRequest  = API_UpdatePasswordRequest;
export const ConfirmRequest  = API_ConfirmRequest;
export const ConfirmResendRequest  = API_ConfirmResendRequest;
export const CreateSecret2FARequest  = API_CreateSecret2FARequest;
export const Enable2FARequest  = API_Enable2FARequest;
export const Disable2FARequest  = API_Disable2FARequest;
export const Check2FARequest  = API_Check2FARequest;

export const ChangeLangRequest  = API_ChangeLangRequest;