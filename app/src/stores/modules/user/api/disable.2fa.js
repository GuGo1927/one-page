import Vue from 'vue';
import ServiceConfig from './config';

export default (password, code) => {

  return Vue.Api.Post(ServiceConfig.service + "/auth/2fa/disable", { password: password }, { headers: { 'X-Auth-2fa': code } });
  
};
