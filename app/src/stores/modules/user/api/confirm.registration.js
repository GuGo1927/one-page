import Vue from 'vue';
import ServiceConfig from './config';

export default (token) => {
	
	return Vue.Api.Get(ServiceConfig.service + "/confirm", { token: token });
	
};
