import Vue from 'vue';
import ServiceConfig from './config';

export default (email) => {
	
	return Vue.Api.Get(ServiceConfig.service + "/confirm/resend", { email: email });
	
};
