import Vue from 'vue';
import ServiceConfig from './config';

export default (email) => {
  
  return Vue.Api.Post(ServiceConfig.service + "/password/forgot", { email: email });
  
};
