import Vue from 'vue';
import ServiceConfig from './config';

export default (key, password, code) => {

  return Vue.Api.Post(ServiceConfig.service + "/auth/2fa/enable", { secret: key, code: code, password: password });
  
};
