import Vue from 'vue';
import ServiceConfig from './config';

export default () => {

    return Vue.Api.Post(ServiceConfig.service + "/logout");

};
