export default {

	isUserAuthenticated: state => {
		return state.isAuthenticated;
	},

	currentUser: state => {
		return state.user;
	},

	connection2faQRCodeUrl: state => {
		return (state.connection[ '2fa' ] != null && state.connection[ '2fa' ].url != undefined) ? state.connection[ '2fa' ].url : "";
	},

	connection2faCode: state => {
		return (state.connection[ '2fa' ] != null && state.connection[ '2fa' ].secret != undefined) ? state.connection[ '2fa' ].secret : "";
	},

	isEnabledConnection2faCode: state => {
		return (state.connection[ '2fa' ] != null && state.connection[ '2fa' ].status != undefined) ? state.connection[ '2fa' ].status : false;
	},

	isInitializedConnection2faStatus: state => {
		return (state.connection[ '2fa' ] == null || state.connection[ '2fa' ].isInitialized == undefined || state.connection[ '2fa' ].isInitialized === false) ? false : true;
	},

}
