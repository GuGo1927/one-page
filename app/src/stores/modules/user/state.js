export default {

	isAuthenticated: false,
	user: null,
	token: null,
	token_expired: 0,
	isValidateToken: false,

	connection: {
		'2fa': null,
	},

}
