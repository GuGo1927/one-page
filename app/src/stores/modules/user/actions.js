import Vue from 'vue';
import * as types from './types';
import * as accounts_types from '../accounts/action.types';
import * as mutation_types from './mutation_types';
import {
	SignInRequest,
	SignUpRequest,
	LogOutRequest,
	RestorePasswordRequest,
	NewPasswordRequest,
	ConfirmRequest,
	CreateSecret2FARequest,
	ConfirmResendRequest,
	Enable2FARequest,
	Disable2FARequest,
	Check2FARequest,
	ChangeLangRequest,
} from './api/methods';

export default {

	/**
	 * Confirm by 2FA code
	 * @param context
	 * @param payload
	 */
	[types.A_2FA_CONFIRM] ( context, payload ) {
		setTimeout (() => {
			context.commit (mutation_types.M_2FA_CONFIRM_SUCCESS);
		}, 3000);
	},

	/**
	 * Set Auth Data From Cookies
	 * @param context
	 * @param payload
	 */
	[types.LOGIN_FROM_COOKIE] (context, payload){
		context.commit (mutation_types.LOGIN_INIT_DATA, payload);
	},

	/**
	 * Resend Confirmation Email
	 * @param context
	 * @param payload
	 */
	[types.A_RESEND_CONFIRMATION] ( context, payload ) {
		ConfirmResendRequest (payload.email).then (
			( response ) => {
				if ( response.status == 204) {
					context.commit (mutation_types.M_RESEND_CONFIRMATION_SUCCESS, {});
				} else {
					context.commit (mutation_types.M_RESEND_CONFIRMATION_FAIL, {
						message: "Something went wrong",
						error: response.data.error
					});
				}
			},

			( error ) => {
				context.commit (mutation_types.M_RESEND_CONFIRMATION_FAIL, {
					error: error
				});
			},
		).catch (( error ) => {

			context.commit (mutation_types.M_RESEND_CONFIRMATION_FAIL, {
				error: error
			});
		});
	},

	/**
	 * Confirm registration
	 * @param context
	 * @param payload
	 */
	[types.CHANGE_LANG] ( context, payload ) {
		ChangeLangRequest (payload.lang).then (
			( response ) => {},
			( error ) => {}
		).catch (( error ) => {});
	},

	/**
	 * Confirm registration
	 * @param context
	 * @param payload
	 */
	[types.A_CONFIRM_REGISTRATION] ( context, payload ) {
		ConfirmRequest (payload.token).then (
			( response ) => {

				console.log ("ACTION CONFIRMED SUCCESS");
				console.log (response);

				if ( response.status == 200 && response.data ) {
					context.commit (mutation_types.M_CONFIRM_REGISTRATION_SUCCESS, {});
				} else {
					context.commit (mutation_types.M_CONFIRM_REGISTRATION_FAIL, { message: "Wrong token!", error: {} });
				}
			},

			( error ) => {

				context.commit (mutation_types.M_CONFIRM_REGISTRATION_FAIL, {
					error: error
				})
			}
		).catch (( error ) => {

			context.commit (mutation_types.M_CONFIRM_REGISTRATION_FAIL, {
				error: error
			});
		});
	},

	/**
	 * User Authorization
	 * @param context
	 * @param payload
	 */

	[types.A_LOGIN] ( context, payload ) {
		SignInRequest (payload.email, payload.password, payload.code).then (
			( response ) => {

				if ( response.status == 200 && response.data ) {

					console.log (response);

					context.commit (mutation_types.M_LOGIN_SUCCESS, {
						token: response.data.token,
						expired: response.data.expired,
						user: response.data.user
					});

				} else {
					context.commit (mutation_types.M_LOGIN_FAIL, {
						message: "Email or Password incorrect",
						error: {}
					});
				}
			},

			( error ) => {

				context.commit (mutation_types.M_LOGIN_FAIL, { error: error })
			}
		).catch (( error ) => {

			context.commit (mutation_types.M_LOGIN_FAIL, {
				error: error
			});
		});
	},

	/**
	 * Register new user
	 * @param context
	 * @param payload
	 */
	[types.A_REGISTER] ( context, payload ) {
		SignUpRequest (payload.username, payload.email, payload.password, payload.lang).then (
			( response ) => {

				console.log ("REGISTER");
				console.log (response);

				if ( response.status == 200 && response.data ) {
					context.commit (mutation_types.M_REGISTER_SUCCESS, response.data);
				} else {
					context.commit (mutation_types.M_REGISTER_FAIL, { message: response.status_message, error: {} });
				}

			},

			( error ) => {

				console.log ("REGISTER 2");
				context.commit (mutation_types.M_REGISTER_FAIL, { error: error });
			}
		).catch (( error ) => {

			console.log ("REGISTER 3");

			context.commit (mutation_types.M_REGISTER_FAIL, {
				error: error
			});
		});
	},

	/**
	 * Update user password
	 * @param context
	 * @param payload
	 */
	[types.A_UPDATE_PASSWORD] ( context, payload ) {
		SignUpRequest (payload.name, payload.email, payload.password).then (
			( response ) => {

				if ( response.status == 200 && response.data ) {
					context.commit (mutation_types.M_REGISTER_SUCCESS, response.data);
				} else {
					context.commit (mutation_types.M_REGISTER_FAIL, { message: response.status_message, error: {} });
				}

			},

			( error ) => {
				context.commit (mutation_types.M_REGISTER_FAIL, { error: error });
			}
		).catch (( error ) => {

			context.commit (mutation_types.M_REGISTER_FAIL, {
				error: error
			});
		});
	},

	/**
	 * Send request to restore password
	 * @param context
	 * @param payload
	 */
	[types.A_RESTORE_PASSWORD] ( context, payload ) {
		RestorePasswordRequest (payload.email).then (
			( response ) => {

				console.log (response);

				if ( response.status == 204 ) {
					context.commit (mutation_types.M_RESTORE_PASSWORD_SUCCESS, response.data);
				} else {
					context.commit (mutation_types.M_RESTORE_PASSWORD_FAIL, {
						message: response.status_message,
						error: response.data.error
					});
				}
			},

			( error ) => {

				console.log ("ERROR");
				console.log (error);
				context.commit (mutation_types.M_RESTORE_PASSWORD_FAIL, { error: error });
			}
		).catch (( error ) => {
			console.log (error);
			context.commit (mutation_types.M_RESTORE_PASSWORD_FAIL, { error: error });
		});
	},

	/**
	 * Set New Password After Restore
	 * @param context
	 * @param payload
	 */
	[types.NEW_PASSWORD] ( context, payload ) {
		NewPasswordRequest (payload.token, payload.password, payload.code).then (
			( response ) => {

				console.log (response);

				if ( response.status == 204 ) {
					context.commit (mutation_types.NEW_PASSWORD_SUCCESS, response.data);
				} else {
					context.commit (mutation_types.NEW_PASSWORD_FAIL, {
						message: response.status_message,
						error: response.data.error
					});
				}
			},

			( error ) => {

				console.log ("ERROR");
				console.log (error);
				context.commit (mutation_types.NEW_PASSWORD_FAIL, { error: error });
			}
		).catch (( error ) => {
			console.log (error);
			context.commit (mutation_types.NEW_PASSWORD_FAIL, { error: error });
		});
	},

	[types.CREATE_SECURE_KEY_2FA] ( context, payload ) {
		CreateSecret2FARequest ().then (
			( response ) => {

				console.log (response);

				if ( response.status == 200 ) {
					context.commit (mutation_types.CREATE_SECRET_2FA_SUCCESS, response.data);
				} else {
					context.commit (mutation_types.CREATE_SECRET_2FA_FAIL, {
						message: response.status_message,
						error: response.data.error
					});
				}
			},

			( error ) => {

				console.log ("ERROR");
				console.log (error);
				context.commit (mutation_types.CREATE_SECRET_2FA_FAIL, { error: error });
			}
		).catch (( error ) => {
			console.log (error);
			context.commit (mutation_types.CREATE_SECRET_2FA_FAIL, { error: error });
		});
	},

	[types.ENABLE_2FA] ( context, payload ) {
		Enable2FARequest (payload.key, payload.password, payload.code).then (
			( response ) => {

				console.log (response);

				if ( response.status == 204 ) {
					context.commit (mutation_types.ENABLE_2FA_SUCCESS);
				} else {
					context.commit (mutation_types.ENABLE_2FA_FAIL, {
						message: response.status_message,
						error: response.data.error
					});
				}
			},

			( error ) => {

				console.log ("ERROR");
				console.log (error);
				context.commit (mutation_types.ENABLE_2FA_FAIL, { error: error });
			}
		).catch (( error ) => {
			console.log (error);
			context.commit (mutation_types.ENABLE_2FA_FAIL, { error: error });
		});
	},

	[types.CHECK_2FA] ( context, payload ) {

		console.log ("CHECK 2FA ACTION");

		Check2FARequest ().then (
			( response ) => {

				console.log (response);

				if ( response.status == 200 ) {
					context.commit (mutation_types.CHECK_2FA_SUCCESS, response.data);
				} else {
					context.commit (mutation_types.CHECK_2FA_FAIL, {
						message: response.status_message,
						error: response.data.error
					});
				}
			},

			( error ) => {

				console.log ("ERROR");
				console.log (error);
				context.commit (mutation_types.CHECK_2FA_FAIL, { error: error });
			}
		).catch (( error ) => {
			console.log (error);
			context.commit (mutation_types.CHECK_2FA_FAIL, { error: error });
		});
	},

	[types.DISABLE_2FA] ( context, payload ) {
		Disable2FARequest (payload.password, payload.code).then (
			( response ) => {

				console.log (response);

				if ( response.status == 204 ) {
					context.commit (mutation_types.DISABLE_2FA_SUCCESS);
				} else {
					context.commit (mutation_types.DISABLE_2FA_FAIL, {
						message: response.status_message,
						error: response.data.error
					});
				}
			},

			( error ) => {

				console.log ("ERROR");
				console.log (error);
				context.commit (mutation_types.DISABLE_2FA_FAIL, { error: error });
			}
		).catch (( error ) => {
			console.log (error);
			context.commit (mutation_types.DISABLE_2FA_FAIL, { error: error });
		});
	},

	[types.A_LOGOUT] ( context ) {
        LogOutRequest().then((response) => {
            if(response.status == 200) {
                context.commit (mutation_types.M_LOGOUT_SUCCESS);               
            }
		}, (error) => {
			console.log(error)
		}).catch((error) => {
            console.log(error)
		});
	},

}
