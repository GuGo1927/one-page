import * as UserMutations from '../modules/user/mutation_types';
import * as AccountActions from '../modules/accounts/action.types';

const GetAccountsAfterLogin = store => {

	console.log("LOAD ACCOUNTS SETUP PLUGIN");

	store.subscribe(mutation => {

		console.log("LOAD ACCOUNTS SUBSCRIBE PLUGIN");

		switch(mutation.type)
		{
			case 'user/'+UserMutations.M_LOGIN_SUCCESS:
			case 'user/'+UserMutations.LOGIN_INIT_DATA:

				console.log("LOAD ACCOUNTS FROM PLUGIN");
				store.dispatch('accounts/'+AccountActions.ACCOUNTS_ACTION_LOAD);

				break;
			default:
				break;
		}
	});
}

export default GetAccountsAfterLogin;