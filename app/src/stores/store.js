import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import state from './state';
import actions from './actions';
import mutations from './mutations';
import getters from './getters';

import UserModule from './modules/user/store';
import MarketsModule from './modules/markets/store';
import TradesModule from './modules/trades/store';
import AccountsModule from './modules/accounts/store';

import LoadAccountsAfterLogin from './plugins/get.accounts.after.login';

export const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  modules: {
      user: UserModule,
      markets: MarketsModule,
      accounts: AccountsModule,
      trades: TradesModule,
  }, plugins: [LoadAccountsAfterLogin]
});
