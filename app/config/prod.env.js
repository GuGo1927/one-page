'use strict'
module.exports = {
  NODE_ENV: '"production"',
  WRAPPER_URL: '"'+process.env.WRAPPER_URL+'"',
  GRAPH_URL: '"'+process.env.GRAPH_URL+'"',
  BACKEND_URL: '"'+process.env.BACKEND_URL+'"',
  USER_SERVICE_URL: '"'+process.env.USER_SERVICE_URL+'"',
  TRADE_SERVICE_URL: '"'+process.env.TRADE_SERVICE_URL+'"',
  CLIENT_DOMAIN: '"'+process.env.CLIENT_DOMAIN+'"',
  PAYMENT_SERVICE_URL: '"'+process.env.PAYMENT_SERVICE_URL+'"',
  GOOGLE_STORAGE_COINMARKET_URL: '"'+process.env.GOOGLE_STORAGE_COINMARKET_URL+'"',
}
